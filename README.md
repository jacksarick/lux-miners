Lux Miners
===

A small game made in JavaScript (built on electron) with the intent of teaching/applying programming skills in JS in a fun way.
![Screenshot](https://gitlab.com/jacksarick/lux-miners/raw/master/images/screenshot.png)

Documentation
===
Base Functions
---
- `spawn_creep(name)` spawns a creep at the base with the name "name"
- `run_creeps()` runs the script of all the base's creep
- `draw_creeps()` draws of all the base's creep, and the base
- `radar()` will return all bases within range of the base. This means that if you detect a base, you can find all of it's creeps, but you cannot detect a creep unless you find the base.

Miner Functions
---
- `move(x, y)` moves that creep x and y
- `execute()` executes that creep's script (Scroll down for infor on creep scripts)
- `draw()` draws that creep
- `latch_on(fountain)` **UNIMPLEMENTED** attempts to latch onto fountain

Creep Scripts
---
Creep's have internal scripts. Theses scripts will be executed on the `execute` function, when it is called from that creep.
Scripts can contain normal JavaScript, but also have access to the creep's functions (see above).
Example script that will cause the creep to go until it hit x=300:
```
if(this.x < 300){
	this.move(1,1);
}
```

To apply this script to a creep, open the dev console (⌘⇧I), and use:
```
base1.creep.miner.script = <above code>;
```
This sets the script of the creep named "miner" in "base1" to whatever code you insert.
This script will then run everytime the screen is updated, before drawing the creep.

Installation instructions
---

- Install node and npm (Both are on homebrew)
- Download the repo as a zip and unzip, or just clone it, and `cd` into the folder. from there, just do `npm install && npm start`