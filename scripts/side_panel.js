module.exports = {
	syntax_highlight: syntax_highlight,
}

// Highlight JSON data
function syntax_highlight(json) {
	// Most of this function courtesy stackoverflow.com/questions/4810841

	// Check type
	if (typeof json != 'string') {
		json = JSON.stringify(json, undefined, 2);
	}

	// Make html compatible
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

	// Serisous regex
	json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {

		// Default class
		var cls = 'number';

		// Things in quotes are either keys or strings
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			}

			else {
				cls = 'string';
			}
		}

		// match booleans and null directly
		else if (/true|false/.test(match)) {
			cls = 'boolean';
		}

		else if (/null/.test(match)) {
			cls = 'null';
		}

		return '<span class="' + cls + '">' + match + '</span>';
	});

	return json;
}