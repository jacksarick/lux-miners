var base_class = require("./base.js");
var structure = require("./structures.js");
var user_console = require("./user_console.js");

// So it can be used in other files
module.exports = {
	World: World
}

//World object
function World (name){
	// Basic constructor
	this.name 	= name;
	this.base 	= {};
	this.structures = [];

	// Create a new miner
	this.new_base = function(x, y, name){
		this.base[name] = new base_class.Base(x, y, name);
		user_console.log("Base \"" + name + "\" created!");
	}

	// Span a new fountain
	// TODO: Make this function random and stuff
	this.spawn_fountain = function(x, y, level){
		this.structures.push(new structure.Fountain(x, y, level));
		console.log("spawned fountain");
	}
}
