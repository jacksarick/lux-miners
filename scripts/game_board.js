var style = require("/Users/Sir/Code/lux-miners/style/board_style.json");

// Board dimensions and the like
var CELL_SIZE = 30,
	CANVAS_WIDTH = 500,
	CANVAS_HEIGHT = 500;

// Clear the canvas
function clear() {
	rect(CANVAS_WIDTH, CANVAS_HEIGHT, 0, 0, style.background);
}

// Draws a rectangle
function rect(w, h, x, y, out_colour, in_colour) {
	ctx.strokeStyle = out_colour;
	ctx.fillStyle = in_colour;
	ctx.beginPath();
	ctx.rect(x, y, w, h);
	ctx.closePath();
	ctx.fill();
	ctx.stroke();
}

// Draws grid square
function grid_square(x, y) {
	rect(CELL_SIZE, CELL_SIZE, x, y, style.grid_outline, style.grid_background);
}

// Draws out grid
function draw_grid(){
	for (var j = 0; j < CANVAS_WIDTH; j += CELL_SIZE) {
		for (var k = 0; k < CANVAS_HEIGHT; k += CELL_SIZE) {
			grid_square(j, k);
		}
	}
}

// If grid is true, return the coords on the grid, not the actual
function get_position(event, grid){
	grid = true;
	var x = event.x;
	var y = event.y;

	var canvas = document.getElementById("canvas");

	x -= canvas.offsetLeft;
	y -= canvas.offsetTop;

	if (grid){
		console.log("x:" + ~~(x/CELL_SIZE) + " y:" + ~~(y/CELL_SIZE));
	}

	else{
		console.log("x:" + x + " y:" + y);
	}
}