var creeps_class = require("./creep.js");
var user_console = require("./user_console.js");
var style = require("../style/creep_style.json");
require("./game_board.js");

// So it can be used in other files
module.exports = {
	Base: Base,
}

//Base object
function Base (x, y, name){
	// Basic constructor
	this.x 		= x;
	this.y 		= y;
	this.name 	= name;
	this.creeps = {};
	this.radius	= 900;

	// Create a new miner
	this.spawn_miner = function(name){
		this.creeps[name] = new creeps_class.Miner(name, this.x, this.y);
		user_console.log("Miner \"" + name + "\" created!");
	}

	// Run the script of all creeps this base spawned
	this.run_creeps = function(){
		for (var creep in this.creeps) {
			this.creeps[creep].execute();
		}
	}

	// Draw all creeps
	this.draw_creeps = function(){
		// Draw self
		this.draw();

		// Draw all the little creepys
		for (var creep in this.creeps) {
			this.creeps[creep].draw();
		}
	}

	this.draw = function(){
		rect(30, 30, this.x, this.y, style.base.outline, style.base.fill);
	}

	// Find all objects close to us
	// Functionally programmed Version
	this.radar = function(){
		var self = this;

		//Not going to go into the math, but checks if an abject is within the radius of "self"
		function in_radius(obj){
			return (Math.pow((self.x - obj.x), 2) + Math.pow((self.y - obj.y), 2) < Math.pow(self.radius, 2));
		};

		// bases get converted to array
		var base_array = Object.keys(main_world.base).map(function(k) { return main_world.base[k] });

		// Return our detected objects
		return base_array.filter(in_radius);
	}
}