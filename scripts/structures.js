var user_console = require("./user_console.js");
var style = require("../style/creep_style.json");
require("./game_board.js");

// So it can be used in other files
module.exports = {
	Fountain: Fountain,
}

//Fountain object
function Fountain (x, y, level){
	// Basic constructor
	this.x 	  = x;
	this.y 	  = y;
	this.level= level;
	this.load = [];

	this.generate = function(level) {
		this.size = level*10;
		for (var i = 0; i < (level*2); i++) {
			// Default resource is lux
			var resource = "lux";

			// Creates a random number between level and 10
			if (Math.floor((Math.random() * (11 - level)) + level) == 10) {
				resource = "quartz";
			}

			// Give the fountain the resource
			this.load.push(resource);
		}
	}

	this.generate(level);

	this.draw = function(){
		rect(this.size, this.size, this.x, this.y, style.fountain.outline, style.fountain.fill);
	}
}