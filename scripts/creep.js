require("./game_board.js");
var style = require("../style/creep_style.json"),
	log = require("./user_console.js");

// So it can be used in other files
module.exports = {
	Creep: Creep,
	Miner: Miner
}


//Basic creep
var Creep = function(name, x, y) {
	// Main constructor
	this.x = x;
	this.y = y;
	this.name = name;
	this.health = 100;
	this.level = 1;
	this.load = {};
	this.ability = {};
	this.script = "//Script goes here";

	// Move the creep
	this.move = function(x_inc, y_inc) {
		// See if speed is too fast
		if (Math.abs(x_inc) > this.ability.speed || Math.abs(y_inc) > this.ability.speed){
			log.error("Speed set for " + this.name + " is too fast!");
		}

		else{
			this.x += x_inc;
			this.y += y_inc;
		}
	}

	// Execute the creep's internal script
	this.execute = function(){
		eval(this.script);
	}
}

// Miner Object
function Miner(name, x, y){
	// Basic constructor
	Creep.call(this, name, x, y);
	this.class = "miner";
	this.ability.speed = 1;

	this.latch_on = function(source){
		// (x - center_x)^2 + (y - center_y)^2 < radius^2
		user.error(this.name = " failed to latch onto " + source.name);
	}

	// Draw the Miner
	this.draw = function(){
		rect(20, 20, this.x, this.y, style.miner.outline, style.miner.fill);
	}
}

