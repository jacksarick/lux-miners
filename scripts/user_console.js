module.exports = {
	log: function(text) { generic_log("log", text); },
	error: function(text) { generic_log("error", text); },
	warning: function(text) { generic_log("warning", text); },
	pass: function(text) { generic_log("pass", text); },
}

function generic_log(style, text){
	var styles = {
		"log": "color: #D2D2D7;",
		"error": "color: #C3274E; font-weight: bold;",
		"warning": "color: #D3C45F;",
		"pass": "color: #B6CA5E;"
	};

	var message = "<tr style='" + styles[style] + "'><td>" + text + "</td></tr>";

	$("#user_console").append(message);
}